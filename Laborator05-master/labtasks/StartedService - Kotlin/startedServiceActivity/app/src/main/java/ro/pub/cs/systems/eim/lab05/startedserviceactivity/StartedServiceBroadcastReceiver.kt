package ro.pub.cs.systems.eim.lab05.startedserviceactivity

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.TextView

class StartedServiceBroadcastReceiver(private val messageTextView: TextView?) : BroadcastReceiver() {

    @SuppressLint("SetTextI18n")
    override fun onReceive(context: Context, intent: Intent) {
        // TODO: exercise 7 - get the action and the extra information from the intent and set the text on the messageTextView
        Log.d("steff", intent.action.toString())
        val action = intent.action
        var data: String? = null
        if (Constants.ACTION_STRING == action) {
            data = intent.getStringExtra(Constants.DATA)
        }
        if (Constants.ACTION_INTEGER == action) {
            data = intent.getIntExtra(Constants.DATA, 0).toString()
        }
        if (Constants.ACTION_ARRAY_LIST == action) {
            data = intent.getStringArrayListExtra(Constants.DATA).toString()
        }
        // ... continue with other actions as needed

        messageTextView?.text = "${messageTextView?.text}\n$data"


        // TODO: exercise 9 - restart the activity through an intent if the messageTextView is not available

    }
}
